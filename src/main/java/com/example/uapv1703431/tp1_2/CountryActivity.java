package com.example.uapv1703431.tp1_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        // get the text view
        TextView countryName = findViewById(R.id.country);

        // get all EditText
        final EditText capitalCity = findViewById(R.id.capitale);
        final EditText language = findViewById(R.id.langue);
        final EditText currency = findViewById(R.id.monnaie);
        final EditText population = findViewById(R.id.population);
        final EditText area = findViewById(R.id.surperficie);

        // get image view
        ImageView image = findViewById(R.id.imageView);

        // get extra value
        Bundle bundle = getIntent().getExtras();
        final String country = bundle.getString("country");


        // get selected country
        Country current = CountryList.getCountry(country);

        // set the data
        countryName.setText(country);

        capitalCity.setText(current.getmCapital());
        language.setText(current.getmLanguage());
        currency.setText(current.getmCurrency());
        population.setText(String.valueOf(current.getmPopulation()));
        area.setText(String.valueOf(current.getmArea() + " km2"));

        String uri = "@drawable/" + current.getmImgFile();
        Log.d("Debug", "image url" + uri);

        image.setImageResource(getResources().getIdentifier(uri, null, getPackageName()));

        Button save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String splittedArea =  area.getText().toString().substring(0, population.getText().toString().length() - 2);
                Log.d("Debug", splittedArea);
                CountryList.save(country, capitalCity.getText().toString(), language.getText().toString(), currency.getText().toString(), Integer.valueOf(population.getText().toString()), Integer.valueOf(splittedArea));
                startActivity(new Intent(CountryActivity.this, MainActivity.class));
            }
        });


    }
}
